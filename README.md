Cipher Wizard
This specific web app is called Cipher Wizard: it aims to allow a user to encode plaintext with a variety of ciphers. The starting code, however, is completely broken, and you'll have to fix it yourself. For example:

Whenever the user types plaintext in the box on the left, the box on the right should auto-update with the ciphertext, depending on what the user selects. Currently, the user cannot type in the left box at all!
Whenever the user selects something in the cipher dropdown, the box on the right should also auto-update. Currently, even though we've already implemented a couple ciphers, the dropdown has no options!
There is a "Switch" button that should move the contents of the right box over to the left. But it currently doesn't do anything!
...and maybe other things!

A completely working example of Cipher Wizard is being served here: https://web.mit.edu/dwhatley/www/cipher-wizard/
Your Tasks
Here are your three tasks:

Replicate the behavior of the "completely working" version. Make sure you've tested out all possible flows and caught all possible bugs!
Implement another feature of your choice! The existing features should give you some inspiration: for example, the "switch" button, or the textbox that appears when you select the "rotN" cipher. You may also notice that the webpage looks suspiciously similar to... what? You might try to implement a feature that exists on that site too :) Be prepared to justify your choices.
Make the webpage look nicer and/or more user-friendly! It currently isn't super eye-appealing; is there something you can do (e.g. CSS styling, or general reordering of elements) you can do to make the app more aesthetically pleasing or intuitive to use? Again, be prepared to justify your choices!

Your code will be evaluated on correctness, clarity, and conciseness.