import React, {Component} from "react";
import {rot13, rotN, base64} from "./Ciphers";

const ciphers = {rot13, rotN, base64};

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    doNothing = () => {
    };

    render() {
        // TODO: I've implemented two ciphers, but this web app
        // doesn't seem to do anything. Please help...

        return (
            <div>
                <h1 className={styles.title}>Cipher Wizard</h1>
                <div style={{display: "flex", flexDirection: "row"}}>
                    <select
                        value="Select"
                        onChange={this.doNothind /* TODO */}
                        style={{width: "40%"}}
                    >
                    </select>
                    <button
                        onClick={this.doNothind /* TODO */}
                        style={{
                            height: "20px",
                            marginLeft: "27%",
                            transform: "translate(-50%, 0%)"
                        }}
                    >
                        Switch
                    </button>
                </div>
                <br/>
                <label>
                    {"Shift: "}
                <input
                    value="Shift"
                    onChange={this.doNothing /* TODO */}
                    style={{width: "10%"}}
                />
                </label>

                <br/>
                <br/>
                <div style={{display: "flex", flexDirection: "row"}}>
          <textarea
              value="Text"
              onChange={this.doNothing() /* TODO */}
              style={{height: "200px", width: "40%"}}
          />
                    <textarea
                        value="Translated text"
                        readOnly
                        style={{
                            height: "200px",
                            width: "40%",
                            marginLeft: "2%",
                            backgroundColor: "powderblue"
                        }}
                    />
                </div>
            </div>
        );
    }
}

export default App;

